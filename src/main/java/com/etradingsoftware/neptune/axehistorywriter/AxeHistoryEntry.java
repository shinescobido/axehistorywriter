package com.etradingsoftware.neptune.axehistorywriter;

import java.util.Date;

public class AxeHistoryEntry {
  private String security;
  private String ccy;
  private String ticker;
  private String side;
  private String source;
  private String sourceDesc;
  private Double amount;
  private Double price;
  private Double spread;
  private Double yield;
  private Date sendDate;

  public String getSecurity() {
    return security;
  }

  public void setSecurity(String security) {
    this.security = security;
  }

  public String getCcy() {
    return ccy;
  }

  public void setCcy(String ccy) {
    this.ccy = ccy;
  }

  public String getTicker() {
    return ticker;
  }

  public void setTicker(String ticker) {
    this.ticker = ticker;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getSourceDesc() {
    return sourceDesc;
  }

  public void setSourceDesc(String sourceDesc) {
    this.sourceDesc = sourceDesc;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getSpread() {
    return spread;
  }

  public void setSpread(Double spread) {
    this.spread = spread;
  }

  public Double getYield() {
    return yield;
  }

  public void setYield(Double yield) {
    this.yield = yield;
  }

  public Date getSendDate() {
    return sendDate;
  }

  public void setSendDate(Date sendDate) {
    this.sendDate = sendDate;
  }

  @Override
  public String toString() {
    return "{AxeHistoryEntry: [security:" + security + ", ccy:" + ccy + ", ticker:" + ticker + ", source:" + source
        + ", sourceDesc:" + sourceDesc + ", amount:" + amount + ", price:" + price + ", spread:" + spread + ", yield:"
        + yield + ", sendDate:" + sendDate + "]}";
  }

  public String getSide() {
    return side;
  }

  public void setSide(String side) {
    this.side = side;
  }



}
