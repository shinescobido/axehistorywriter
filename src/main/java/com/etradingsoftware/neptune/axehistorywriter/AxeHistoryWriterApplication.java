package com.etradingsoftware.neptune.axehistorywriter;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import javax.annotation.PreDestroy;
import org.bson.BsonBinary;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import com.mongodb.client.ChangeStreamIterable;
import com.mongodb.client.MongoChangeStreamCursor;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;

@SpringBootApplication
@PropertySource("classpath:application.properties")
@EnableAutoConfiguration
public class AxeHistoryWriterApplication implements CommandLineRunner {

  @Value("${mongo.db.uri}")
  private String dbURI;

  @Value("${mongo.db.name}")
  private String dbName;

  @Value("${mongo.db.collection}")
  private String dbCollection;

  @Value("${app.resumeToken:#{null}}")
  private String resumeTokenStr;

  private static final String HEADER = "header";
  private static final String PAYLOAD = "payload";
  private static final String BODY = "body";
  private static final String METADATA = "metadata";

  private BsonDocument resumeToken;
  private MongoChangeStreamCursor<ChangeStreamDocument<Document>> cursor;

  private static Logger LOG = LoggerFactory.getLogger(AxeHistoryWriterApplication.class);

  private DbStrategy mongo = new WriteToMongoStrategy();
  
  private int ctr = 0;
  private long totalTime = 0;

  public static void main(String[] args) {
    LOG.info("STARTING THE APPLICATION");
    SpringApplication.run(AxeHistoryWriterApplication.class, args);
    LOG.info("APPLICATION FINISHED");
  }

  public void run(String... args) {
    LOG.info("EXECUTING : command line runner");

    try {
      MongoCollection<Document> collection =
          MongoClients.create(dbURI).getDatabase(dbName).getCollection(dbCollection, Document.class);

      Bson match = Aggregates.match(Filters.in("operationType", Arrays.asList("insert", "update")));

      ChangeStreamIterable<Document> fullDocument =
          collection.watch(Arrays.asList(match)).fullDocument(FullDocument.UPDATE_LOOKUP);


      if (resumeTokenStr != null) {
        LOG.info("resuming using token " + resumeTokenStr);
        BsonDocument existingResumeToken = new BsonDocument("_data", new BsonBinary(UUID.fromString(resumeTokenStr)));
        cursor = fullDocument.resumeAfter(existingResumeToken).cursor();
      } else {
        cursor = fullDocument.cursor();
      }

      while (cursor.hasNext()) {
        Instant startTime = Instant.now();
        resumeToken = cursor.getResumeToken();
        ChangeStreamDocument<Document> document = cursor.next();
        ctr++;
        if (null != document.getFullDocument()) {
         
          processData(document.getFullDocument());

        } else {
          LOG.error("FullDocument is null.");
        }
        Instant endInstant = Instant.now();
        Duration elapseTime = Duration.between(startTime, endInstant);
        totalTime += elapseTime.toMillis();
        
        LOG.info("Count: " + ctr);
        LOG.info("Elapsed: " + totalTime);
      }

    } catch (Exception e) {
      LOG.error(e.getMessage());
    }
  }


  private void processData(Document docu) {
    AxeHistoryEntry entry = new AxeHistoryEntry();
    entry.setSecurity(getTagValue(docu, HEADER, "security"));
    entry.setCcy(getTagValue(docu, METADATA, "ccy"));
    if (entry.getCcy() == null) {
      entry.setCcy(getTagValue(docu, PAYLOAD, "15"));
    }
    entry.setSide(getTagValue(docu, HEADER, "side"));
    entry.setSource(getTagValue(docu, HEADER, "source"));
    entry.setSourceDesc(getTagValue(docu, METADATA, "dealerName"));
    entry.setAmount(getDoubleValue(docu, PAYLOAD, "27"));
    entry.setPrice(getDoubleValue(docu, PAYLOAD, "44"));
    entry.setYield(getDoubleValue(docu, PAYLOAD, "236"));
    entry.setSpread(getDoubleValue(docu, PAYLOAD, "218"));
    entry.setSendDate(getDateValue(docu, PAYLOAD, "52"));
    //
    mongo.write(entry);
  }

  private String getTagValue(Document docu, String key, String tag) {
    Document keyObject = (Document) docu.get(key);
    if (key.equals(PAYLOAD)) {
      return getTagValue(keyObject, BODY, tag);
    }

    return keyObject != null ? keyObject.getString(tag) : null;
  }

  private Date getDateValue(Document docu, String key, String tag) {
    Document keyObject = (Document) docu.get(key);
    if (key.equals(PAYLOAD)) {
      return getDateValue(keyObject, HEADER, tag);
    }
    return keyObject.getDate(tag);
  }

  private Double getDoubleValue(Document docu, String key, String tag) { 
    Document keyObject = (Document) docu.get(key);
    if (key.equals(PAYLOAD)) {
      return getDoubleValue(keyObject, BODY, tag);
    }

    return keyObject.getDouble(tag);
  }


  @PreDestroy
  public void onExit() {
    LOG.info("Shutdown initiated.");
    try {
      if (cursor != null) {
        cursor.close();
        LOG.info("Last resumeToken:" + resumeToken.toJson());
      }
      Thread.sleep(5 * 1000);

    } catch (InterruptedException e) {
      LOG.error("", e);;
    }
    LOG.info("BYE!");
  }
}
