package com.etradingsoftware.neptune.axehistorywriter;

public interface DbStrategy {

  public void write(AxeHistoryEntry axeHistoryRequests);

  public void closeClient();
}
