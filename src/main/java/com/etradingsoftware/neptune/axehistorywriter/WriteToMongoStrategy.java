package com.etradingsoftware.neptune.axehistorywriter;

import org.bson.Document;
import com.google.gson.Gson;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;

public class WriteToMongoStrategy implements DbStrategy {
  private MongoCollection<Document> collection;

  private Gson gson = new Gson();

  public WriteToMongoStrategy() {
    collection = MongoClients.create("mongodb://localhost:27017/?replicaSet=rs0").getDatabase("TEST_DB")
        .getCollection("test_axe_history", Document.class);
  }


  @Override
  public void write(AxeHistoryEntry entry) {
    // System.out.println(entry);
    collection.insertOne(Document.parse(gson.toJson(entry)));
  }

  @Override
  public void closeClient() {
   
  }

}
